#!/bin/bash

read -p "Enter psql pass : " psqlPassword
read -p "Enter psql port : " psqlPort

#install db
apt update
apt install postgresql postgresql-contrib -y
pg_ctlcluster 12 main start

#install golang
wget https://golang.org/dl/go1.17.1.linux-amd64.tar.gz
tar -C /usr/local -xzf go1.17.1.linux-amd64.tar.gz
export PATH=$PATH:/usr/local/go/bin

#setup db
cd /tmp && sudo -u postgres psql -U postgres -d postgres -c "alter user postgres with password '$psqlPassword';"
sed -i 's/peer/md5/g' /etc/postgresql/12/main/pg_hba.conf
sed -i -e "s/#listen_addresses = 'localhost'/listen_addresses = '*'/g" /etc/postgresql/12/main/postgresql.conf
sed -i -e "s/127.0.0.1\/32/0.0.0.0\/0/g" /etc/postgresql/12/main/pg_hba.conf
sed -i -e "s/port.*/port = $psqlPort/" /etc/postgresql/12/main/postgresql.conf

service postgresql restart

#add cron task
echo "#!/bin/bash" > /tmp/backup.sh
echo "psqlPassword=\"$psqlPassword\"" >> /tmp/backup.sh
#backup dir
echo "dumpDir=\"/tmp/backup\"" >> /tmp/backup.sh
echo "test -d $dumpDir || mkdir -p $dumpDir" >> /tmp/backup.sh
echo "pg_dump --clean \"host=localhost port=5432 dbname=postgres user=postgres password=\$psqlPassword\" > \${dumpDir}/postgres.sql" >> /tmp/backup.sh
chmod +x /tmp/backup.sh
mv /tmp/backup.sh /root/backup.sh

echo "0 0 * * * /root/backup.sh" >> /var/spool/cron/crontabs/root
service cron restart

#
# wget https://gitlab.com/microser/pp/-/raw/master/first/up.sql
# wget https://gitlab.com/microser/pp/-/raw/master/first/back_linux_amd64

# psql "host=localhost port=5432 dbname=postgres user=postgres password=$psqlPassword" < up.sql
# chmod +x back_linux_amd64
# mv back_linux_amd64 /opt

# cd /opt && ./back_linux_amd64
