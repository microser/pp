#!/bin/bash
#read -p "Enter psql password : " psqlPassword
psqlPassword="pass"
dumpFile="/tmp/backup/postgres.sql"

if test -f "$dumpFile"; then
    psql "host=localhost port=5432 dbname=postgres user=postgres password=$psqlPassword" -f $dumpFile postgres
else
    echo "$dumpFile not found!"
fi
