#!/bin/bash

read -p "Enter BotToken : " botToken


botDir="/opt/bot"
ip=`hostname -I | awk '{ print $1 }'`

apt update && aptinstall git openssl -y

#install golang
wget https://golang.org/dl/go1.15.8.linux-amd64.tar.gz
tar -C /usr/local -xzf go1.15.8.linux-amd64.tar.gz
export PATH=$PATH:/usr/local/go/bin

##
mkdir -p $botDir && rm -rf *

openssl req -x509 -nodes -days 365 -newkey rsa:2048 -subj "/C=US/ST=Oregon/L=Portland/O=Company Name/OU=Org/CN=$ip" -keyout $botDir/key.pem -out $botDir/cert.pem

##
cd $botDir

#bot token init
wget https://api.telegram.org/bot$botToken/setWebhook?url=https://$ip:8443

wget https://gitlab.com/microser/pp/-/raw/master/bot/bot_linux_amd64
chmod +x bot_linux_amd64 && ./bot_linux_amd64

##success
