DROP TABLE IF EXISTS settings CASCADE;
DROP TABLE IF EXISTS users CASCADE;
DROP TABLE IF EXISTS statistics CASCADE;
DROP TABLE IF EXISTS refferals CASCADE;
DROP TABLE IF EXISTS offers_my CASCADE;
DROP TABLE IF EXISTS offers_partners CASCADE;
DROP TABLE IF EXISTS offers_sub_ids CASCADE;
DROP TABLE IF EXISTS payouts CASCADE;
DROP TABLE IF EXISTS visitors CASCADE;
DROP TABLE IF EXISTS offers CASCADE;
DROP TABLE IF EXISTS upsells CASCADE;
DROP TABLE IF EXISTS orders CASCADE;

CREATE TABLE IF NOT EXISTS settings
(
    timezone                  TEXT   NOT NULL,
    service_percent           BIGINT NOT NULL,
    refferal_system_mode      BIGINT NOT NULL,
    refferal_invite_limit     BIGINT NOT NULL,
    refferal_percent          BIGINT NOT NULL,
    admin_tg_login            BIGINT NOT NULL,
    admin_notify              BIGINT NOT NULL,
    auto_activate_members     BIGINT NOT NULL,
    activate_members_interval BIGINT NOT NULL,
    auto_moderate_offers      BIGINT NOT NULL,
    moderate_offers_interval  BIGINT NOT NULL,
    track_domain              TEXT   NOT NULL,
    pay_domain                TEXT   NOT NULL,
    pay_form_domain           TEXT   NOT NULL,
    service_balance           BIGINT NOT NULL,
    sum_service_balance       BIGINT NOT NULL,
    service_status            BIGINT NOT NULL,
    bot_name                  TEXT   NOT NULL
--     created_at   TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
--     updated_at   TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);

insert into public.settings (timezone, service_percent, refferal_system_mode, refferal_invite_limit, refferal_percent,
                             admin_tg_login, admin_notify, auto_activate_members, activate_members_interval,
                             auto_moderate_offers, moderate_offers_interval, track_domain, pay_domain, pay_form_domain,
                             service_balance, sum_service_balance, service_status, bot_name)
values ('Europe/Moscow', 45, 1, 1, 4, 803038656, 1, 1, 1, 1, 1, 'track.ru', 'pay.ru', 'http://192.168.1.44:8080/',
        56787679, 156787679, 1, 'Topchiki_bot');

CREATE TABLE IF NOT EXISTS users
(
    id                    SERIAL PRIMARY KEY,
    tg_login              BIGINT NOT NULL,
    username              TEXT   NOT NULL,
    service_percent       BIGINT NOT NULL,
    refferal_percent      BIGINT NOT NULL,
    refferal_invite_limit BIGINT NOT NULL,
    allow_inviting        BIGINT NOT NULL,
    invite_code           TEXT   NOT NULL,
    notifications         BIGINT NOT NULL,
    timezone              TEXT   NOT NULL,
    track_domain          TEXT   NOT NULL,
    pay_domain            TEXT   NOT NULL,
    balance               float  NOT NULL,
    wallet                BIGINT NOT NULL,
    status                BIGINT NOT NULL,
    created_at            TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    updated_at            TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);

CREATE TYPE visit_type AS ENUM ('track', 'pay', 'unique');
CREATE TABLE IF NOT EXISTS statistics
(
    visitor_id BIGINT NOT NULL,
--     user_id    BIGINT NOT NULL,
--     offer_id   BIGINT NOT NULL,
--     upsell_id  BIGINT NOT NULL          DEFAULT 0,
--     sub_id     BIGINT NOT NULL          DEFAULT 0,
    type_visit visit_type,
    created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS offers
(
    id          SERIAL PRIMARY KEY,
    author_id   BIGINT NOT NULL,
    site_name   TEXT   NOT NULL,
    category    BIGINT NOT NULL,
    domain      TEXT   NOT NULL,
    name        TEXT   NOT NULL,
    price       BIGINT NOT NULL,
    percent     BIGINT NOT NULL,
    success_url TEXT   NOT NULL,
    failed_url  TEXT   NOT NULL,
    ismoderated BIGINT NOT NULL          DEFAULT 0,
    incatalog   BIGINT NOT NULL          DEFAULT 0,
    created_at  TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    updated_at  TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP

);

CREATE TABLE IF NOT EXISTS visitors
(
    id          SERIAL PRIMARY KEY,
    user_id     BIGINT NOT NULL,
    offer_id    BIGINT NOT NULL,
    upsell_id   BIGINT NOT NULL          DEFAULT 0,
    sub_id      BIGINT NOT NULL          DEFAULT 0,
    country     TEXT   NOT NULL,
    fingerprint TEXT   NOT NULL,
    created_at  TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    updated_at  TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS payouts
(
    id         SERIAL PRIMARY KEY,
    user_id    BIGINT NOT NULL,
    sum_payout BIGINT NOT NULL,
    status     BIGINT NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);


CREATE TABLE IF NOT EXISTS upsells
(
    id            SERIAL PRIMARY KEY,
    offer_id      BIGINT NOT NULL,
    upsell_number SERIAL,
    name          TEXT   NOT NULL,
    price         BIGINT NOT NULL,
    percent       BIGINT NOT NULL,
    success_url   TEXT   NOT NULL,
    failed_url    TEXT   NOT NULL
);
-- мои офферы
CREATE TABLE IF NOT EXISTS offers_my
(
    id            SERIAL PRIMARY KEY,
    user_id       BIGINT NOT NULL,
    offer_id      BIGINT NOT NULL,
    upsell_number BIGINT NOT NULL          DEFAULT 0,
    pay_link      TEXT   NOT NULL,
    ismoderated   BIGINT NOT NULL          DEFAULT 0,
    incatalog     BIGINT NOT NULL          DEFAULT 0,
    created_at    TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    updated_at    TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);
-- я партнер
CREATE TABLE IF NOT EXISTS offers_partners
(
    id           SERIAL PRIMARY KEY,
    user_id      BIGINT NOT NULL,
    offer_id     BIGINT NOT NULL,
    upsell_id    BIGINT NOT NULL DEFAULT 0,
    sub_id       BIGINT NOT NULL DEFAULT 0,
    partner_link TEXT   NOT NULL,
    percent      BIGINT NOT NULL,
    deleted      BIGINT NOT NULL DEFAULT 0

);

-- subids
CREATE TABLE IF NOT EXISTS offers_sub_ids
(
    id                 SERIAL PRIMARY KEY,
    user_id            BIGINT NOT NULL,
    offer_id           BIGINT NOT NULL,
    name               TEXT   NOT NULL,
    subid_partner_link TEXT   NOT NULL

);
-- refferals
CREATE TABLE IF NOT EXISTS refferals
(
    id               SERIAL PRIMARY KEY,
    user_id          BIGINT NOT NULL,
    refferal_id      BIGINT NOT NULL,
    refferal_percent BIGINT NOT NULL,
    created_at       TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    updated_at       TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);


CREATE TABLE IF NOT EXISTS orders
(
    id            BIGSERIAL PRIMARY KEY,
    user_id       BIGINT NOT NULL,
    offer_id      BIGINT NOT NULL,
    upsell_id     BIGINT NOT NULL,
    visitor_id    BIGINT NOT NULL,
    price         float  NOT NULL,
    user_price    float  NOT NULL,
    partner_price float  NOT NULL,
    status        BIGINT NOT NULL,
    created_at    TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    updated_at    TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);
