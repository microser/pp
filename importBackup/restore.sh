#!/bin/bash
read -p "Enter psql password : " psqlPassword

mkdir -p /tmp/backup/
cd /tmp/backup/ && rm -rf *

##
wget https://gitlab.com/microser/pp/-/raw/master/importBackup/postgres.sql
mv postgres.sql /tmp/backup/postgres.sql

dumpFile="/tmp/backup/postgres.sql"

if test -f "$dumpFile"; then
    psql "host=localhost port=5432 dbname=postgres user=postgres password=$psqlPassword" -f $dumpFile postgres
else
    echo "$dumpFile not found!"
fi
